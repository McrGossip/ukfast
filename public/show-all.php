<!doctype html>
<?php
error_reporting(0);
require __DIR__.'/../vendor/autoload.php';
use PokePHP\PokeApi;
include 'classLib/helperClass.php';
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pokédex</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/theme.min.css">
    </head>
    <body>
        <header>
         <h1>UK Fast test</h1>
        </header>
            <div class="container">                   
                <div class="container-inner">
                    <?php
                        $api = new PokeApi;                     // Instantiate an object
                        $switchHelper = new helperClass();
                        
                        $pokemon    = $api->pokedex(1);         //  Make an api call for a resource
                        $data       = json_decode($pokemon);    //  convert the json response to array

                        foreach ($data->pokemon_entries as $row) {
                            $pokemon = [
                                'name'  => $row->pokemon_species->name,
                                'entry' => $row->entry_number
                            ];
                            
                            $name =  $row->pokemon_species->name;
                            
                            if(!isset($row->entry_number)){
                                continue; // If there's no entry number then
                            }
                            
                            $resourceId = $row->entry_number;
                            $imgId = $switchHelper->prefixNumber($resourceId);
                            $biggerImageUri =   "http://media.bisafans.de/6af690d/pokemon/artwork/$imgId.png";
                             
                            echo "<div class='small_pokemon_container small_pokemon_card fadeIn'>";    // here's our div container
                            echo "<a href='index.php?pokenumber=$row->entry_number' title='Press to learn more about $name'>";    // here's our div container
                            echo nl2br ("<img src='$biggerImageUri' width='100px' />" . "\n");  //  display an image
                            echo nl2br ($name . "\n");
                            echo '</div></a>';
                        }
                    ?>;
                </div>
            </div>
    </body>
</html>