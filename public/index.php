<?php
error_reporting(0);
require __DIR__.'/../vendor/autoload.php';
use PokePHP\PokeApi;
include 'classLib/helperClass.php';
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pokédex</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/theme.min.css">
    </head>
    <body>
        <header>
         <h1>UK Fast test</h1>
        </header>
            <div class="show_all_button">
                    <a href="/show-all.php"><button type="button" class="btn btn-secondary btn button-inner-left">Show All</button></a>
                    <form action = "<?php $_PHP_SELF ?>" method = "POST">
                        <input type="hidden"  value="<?php rand(1,802);?>" name="pokenumber" /> 
                        <input class="btn btn-info btn button-inner-right" type = "submit" value="Random"  />
                    </form>
            </div>
        
            <div class="form">
                <form action = "<?php $_PHP_SELF ?>" method = "POST">
                     <input type="text"  placeholder="Enter Pokémon ID: #" class="input_box"  name="pokenumber" /> 
                     <input class="btn btn-light btn" type = "submit" value="Search!" />
                </form>
            </div>
            
            <?php
            
            $api = new PokeApi; // Instantiate an object
            $switchHelper = new helperClass();

            if (isset($_REQUEST['pokenumber']) && is_numeric(($_REQUEST['pokenumber']))){
                $resourceId = $_REQUEST['pokenumber'];
            } else {
                $resourceId = (rand(1,802)); // If no Id is submitted let's pick a random pokemon to display.
            }
            
            $totalNumberOfPokemon = '802';
            if ($resourceId > $totalNumberOfPokemon) {
                echo '<div class="out_of_range">';
                echo 'Please select a number less than 802'; // There's a few after 802 but they're missing attributes.
                echo '</div>';
                exit();
            }

            $pokemon    = $api->pokemon($resourceId);      //  Make an api call for a resource
            $data       = json_decode($pokemon);           //  convert the json response to array
           
            $imgId = $switchHelper->prefixNumber($resourceId);

            $biggerImageUri =   "http://media.bisafans.de/6af690d/pokemon/artwork/$imgId.png";
            $pokemonName    =   $data->name;
            $height         =   $data->height;
            $weight         =   $data->weight;
            $divClass       =   "<div class='pokemon_container large_pokemon_card fadeIn'>";
            
            echo "$divClass";   //  here's our div container
            echo "<img src='$biggerImageUri' width='200px' />" . "\n";          //  display an image
            echo "<h1>" . nl2br ($pokemonName  . "\n") . "</h1>";               //  pokemon name
            echo "Pokemon ID: #" . $resourceId;                                 //  Id Number
            echo "<h3>Height ~ " . nl2br ($height  . "\n");                     //  height
            echo "Weight ~ " . $weight  . "\n" . "</h3>";                       //  weight
            echo "Abilities: ";                                                 //  just the Abilities label
            foreach ($data->abilities as $row) {
                $pokemon = [
                    'name'  => $row->ability->name
                ];
                echo $row->ability->name . ", ";
            }
            echo "</div>";
            
            ?>;
    </body>
</html>