<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of helper
 *
 * @author Craig
 */
class helperClass {
    
    function prefixNumber($resourceId){
    
// The switch statement below takes care of the image prefix we need.
        switch ($resourceId) {
            case ($resourceId <= '9'):
                $prefix = '00';
                break;
            case ($resourceId > '99'):
                $prefix = '';
                break;
            default:
                $prefix = '0';
        }
        $imgId = $prefix . $resourceId;
        return $imgId;   
    }
    
}
